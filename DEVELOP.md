# Develop

## Install

Check [install doc](./server/README.md#setup) for details.

## Testing

Tests are available in both [server](./server/README.md#testing) and [viewer](./viewer/README.md#testing). If you're working on bug fixes or new features, please make sure to add appropriate tests to keep GeoVisio level of quality.

## Make a release

```bash
git checkout develop

cd server/
vim setup.cfg
# Change metadata.version

cd ../viewer/
vim package.json
# Change version
npm run doc

cd ../
git add *
git commit -m "Release x.x.x"
git tag -a x.x.x -m "Release x.x.x"
git push origin develop
git checkout main
git merge develop
git push origin main --tags
```
