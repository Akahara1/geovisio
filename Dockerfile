FROM node:16.14 AS builder
WORKDIR /src
COPY ./viewer/package*.json ./
RUN npm update -g
RUN npm update -g npm
RUN npm install --legacy-peer-deps
# Build viewer
COPY ./viewer ./
RUN npm run build

# ---
FROM python:3.9

# Environment variables
ENV DB_URL="postgres://user:pass@host/dbname"
ENV FS_URL="osfs:///data/360app"
ENV MODELS_FS_URL="/data/360models"
ENV SERVER_NAME="localhost:5000"
ENV SECRET_KEY="see_readme"
ENV YOLOV6_PATH="/opt/YOLOv6"
ENV BACKEND_MODE="api"

# install system dependencies
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6 -y

# download YOLOv6, an object detection AI
RUN git clone https://github.com/meituan/YOLOv6 /opt/YOLOv6

# Create folders
RUN mkdir -p /opt/360app/server /opt/360app/viewer /data/360app

# Install Python dependencies
WORKDIR /opt/360app/server
COPY ./server/requirements.txt ./
RUN pip install -r ./requirements.txt && \
    pip install waitress && \
    pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cpu && \
    pip install -r /opt/YOLOv6/requirements.txt

# Add server source files
WORKDIR /opt/360app
COPY ./docker-entrypoint.sh ./
RUN chmod +x ./docker-entrypoint.sh
COPY ./server ./server/
COPY ./images ./images/

# Add viewer build result
COPY --from=builder /src/build /opt/360app/viewer/build
COPY ./viewer/demo /opt/360app/viewer/demo
COPY ./viewer/public /opt/360app/viewer/public

# Expose service
EXPOSE 5000
ENTRYPOINT ["./docker-entrypoint.sh"]
