# GeoVisio / Server

Server is the component handling hosting of pictures. It analyzes and serves your geolocated pictures through an API.

## Dependencies

GeoVisio best runs with a Linux server and needs these components:

- Python 3.9, higher versions are currently unsupported by blurring algorithms
- A [PostgreSQL](https://www.postgresql.org/) 10+ database with [PostGIS](https://postgis.net/) 3+ extension

## Setup

### Database configuration

If you're starting with a fresh PostgreSQL install (and not sure about what to do), you need to change these parameters in order to have GeoVisio working:

- Create a new database with this command

```bash
sudo su - postgres -c "psql -c 'CREATE DATABASE geovisio'"
```

- Change the `listen_adresses` parameter in `postgresql.conf` to be `'*'`
- Add in `pg_hba.conf` file the following line (considering your username is `postgres` and database `geovisio`)

```
host	geovisio	postgres	172.17.0.0/24	trust
```

Once your database is ready, you can go through these steps

### Basic install

```bash
# Set your config through environment variables
export DB_URL="postgres://postgres@172.17.0.1/geovisio" # May change according to your config
export FS_URL="osfs:///my/pic/dir" # You have to change the /my/pic/dir path in the command to your actual pictures folder

# Enable Python environment
python3 -m venv env
source ./env/bin/activate
pip install -r requirements.txt

# Command to run pictures processing
# It will look through your pictures folder and create appropriate metadata in database
FLASK_APP="src" FLASK_ENV=development flask process-sequences

# Command to start API
FLASK_APP="src" FLASK_ENV=development flask run
```

### Alternative: Docker install

```bash
export DB_URL="postgres://postgres@172.17.0.1/geovisio" # May change according to your config

# Retrieve the stable Docker image
# An image with latest developments is also available: panieravide/geovisio:develop
docker pull panieravide/geovisio

# Command to run pictures processing (metadata parsing, thumbnails/tiles/blurring processing)
# This can take several minutes according to your pictures folder size
# You have to change the /my/pic/dir path in the command to your actual pictures folder
docker run \
	-e DB_URL \
	-v /my/pic/dir:/data/360app \
	panieravide/geovisio \
	process-sequences

# Command to start API
docker run --rm --name=geovisio \
	-p 5000:5000 \
	-e DB_URL \
	-v /my/pic/dir:/data/360app \
	panieravide/geovisio \
	api
```

### Testing

Various unit tests are available to ensure the server is well working. You can run them by following these steps:

- In your `config.py` file, add a `TEST_DB_URL` parameter, which follows the `DB_URL` parameter format, so you can use a dedicated database for testing
- Run `pytest` command

### Deploy in production

When deploying in a production environment, you should at some point plan to regularly run these operations:

- PostgreSQL database REINDEX / VACUUM (~ once per day/week)
- Process sequences command (~ every 5 minutes)

### Uninstall / cleaning

If you want to uninstall or clean-up GeoVisio files, you can run the following command:

```bash
FLASK_APP="src" FLASK_ENV=development flask cleanup
```

This will delete database GeoVisio tables and derivated pictures files (`gvs_derivates` folder). It will __not delete__ your original pictures files.


## Configuration

Server configuration can be set through different methods:

- By setting `config.py` file (see [example](./config.example.py))
- By using system environment variables

Available parameters are :

- `DB_URL` (mandatory) : [connection string](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING) to access the PostgreSQL database. You can alternatively use a set of `DB_PORT`, `DB_HOST`, `DB_USERNAME`, `DB_PASSWORD`, `DB_NAME` parameters to configure database access.
- `SERVER_NAME` (optional) : publicly visible HTTP API hostname (see [Flask SERVER_NAME parameter](https://flask.palletsprojects.com/en/2.1.x/config/#SERVER_NAME) for details), for example : `myserver.net:3000`
- `FS_URL` (mandatory) : file system and directory to use for reading and storing pictures (in [PyFilesystem format](https://docs.pyfilesystem.org/en/latest/openers.html)), for example : `osfs:///path/to/pic/dir` (disk storage), `s3://mybucket/myfolder?endpoint_url=https%3A%2F%2Fs3.fr-par.scw.cloud&region=fr-par` (S3 storage)
- `BLUR_STRATEGY` (optional) : activate blurring for all new picture uploads. See [src/blur/](./src/blur/) for more info.
- `BACKEND_MODE` (optional) : sets the mode to run GeoVisio into ("api" for web API, "process-sequences" for checking and processing incoming pictures). This is used mainly for Kubernetes / Dockerized deployments to avoid passing arguments to container.

## Usage

### Prepare your pictures

GeoVisio has several prerequisites for a pictures to be accepted, mainly concerning availability of some [EXIF tags](https://en.wikipedia.org/wiki/Exif). A picture __must__ have the following EXIF tags defined:

- GPS coordinates with `GPSLatitude, GPSLatitudeRef, GPSLongitude, GPSLongitudeRef`
- Image orientation with `GPSImgDirection`
- Date, either with `GPSTimeStamp` or `DateTimeOriginal`

The following EXIF tags are recognized and used if defined, but are __optional__:

- Milliseconds in date with `SubSecTimeOriginal`
- If picture is 360° / spherical with `GPano:ProjectionType`
- Camera model with `Make, Model`
- Camera focal length (to get precise field of view) with `FocalLength`

Note that GeoVisio now accepts both __360° and classic/flat pictures__ (versions <= 1.2.0 only supported 360° pictures).

### Organize your pictures

For GeoVisio to recognize your pictures and sequences, your instance root folder (`FS_URL` parameter) should contain a list of subfolders (one per sequence), and each subfolder should contain a list of pictures (JPEG files, read in alphabetical order). As an example of tree hierarchy:

- Main folder (according to your `FS_URL` parameter)
  - `my_seq_1` : one sequence folder
    - `my_pic_1.jpg`
    - `my_pic_2.jpg`
    - `my_pic_3.jpg`
    - ...
  - `my_seq_2` : another sequence
    - And its pictures...
  - ...

Once this directory is ready, the `process-sequences` will look through it and parse necessary metadata. Note that original pictures and folder structure __will not be changed__. Although, GeoVisio will add various automatically generated files, all prefixed with `gvs_` so you can easily clean-up if necessary.

Eventually, if you want to clear database and delete derivate versions of pictures (it __doesn't__ delete original pictures), you can use the `cleanup` command:

```
FLASK_APP="src" FLASK_ENV=development flask run
```

### API

API routes are documented at [localhost:5000/apidocs](http://localhost:5000/apidocs). It allows you to retrieve sequence and picture metadata, and also access to raw or edited versions of your pictures. API is also compliant with [STAC API scheme](https://github.com/radiantearth/stac-api-spec) through `/stac/` route.

