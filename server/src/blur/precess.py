import os
import urllib.request
from PIL import Image
import numpy as np

"""
Image preprocessing and AI models utilities
"""

baseModelsDir = None


def setModelsDir(modelsDir):
    global baseModelsDir
    baseModelsDir = modelsDir


def getModelFile(modelName, downloadUrl=None):
    """Returns the model file associated with the model's name.

    setModelsDir() must have been called before this method.
    If necessary, this method will download the model file.

    Parameters
    ----------
    modelName : str
        the model's name
    downloadUrl : str | None
        if the model file does not exist it will be downloaded.
        if downloadUrl is None an exception will be thrown.

    Returns
    -------
    str
        the model file path
    """
    if baseModelsDir is None:
        raise RuntimeError("setModelsDir() has not been called yet")
    if not os.path.exists(baseModelsDir):
        os.mkdir(baseModelsDir)
    downloadPath = os.path.join(baseModelsDir, modelName)
    if not os.path.exists(downloadPath):
        if downloadUrl is None:
            raise Exception("Model %s does not exist and cannot be downloaded" % modelName)
        print("Downloading model", modelName)
        urllib.request.urlretrieve(downloadUrl, downloadPath + ".tmp")
        os.rename(downloadPath + ".tmp", downloadPath)
    return downloadPath


def iterQuadSubSections(quadSize, blockSize, overlap):
    """Yields a list of boxes (x,y,w,h) spanning over the given quad.

    Parameters
    ----------
    quadSize : (int, int)
        the quad's size
    blockSize : (int, int)
        the size of a spanning block
    overlap : int
        if positive, spanning blocks will overlap by the given amount, on both axes

    Yields
    ------
    (
        x1 : int,
        y1 : int,
        x2 : int,
        y2 : int
    )
    """
    qw, qh = quadSize
    bw, bh = blockSize
    x = 0
    while x+bw <= qw:
        y = 0
        while y+bh <= qh:
            yield (x, y, x+bw, y+bh)
            y += bh-overlap
        x += bw-overlap
    if qw%(bw-overlap)-overlap != 0:
        y = 0
        while y+bh <= qh:
            yield (qw-bw, y, qw, y+bh)
            y += bh-overlap
    if qh%(bh-overlap)-overlap != 0:
        x = 0
        while x+bw <= qw:
            yield (x, qh-bh, x+bw, qh)
            x += bw-overlap
    if qw%(bw-overlap)-overlap != 0 and qh%(bh-overlap)-overlap != 0:
        yield (qw-bw, qh-bh, qw, qh)


def computeBestDownscalling(imageSize, targetQuadsSize, margin=.1, maxReduction=1/40):
    """Computes the best size to resize an image to when trying to split
    an image into multiple quads.

    This algorithm will produce a size close to an integer multiple of the
    target quad size, never less than it, while trying to preserve the orginal
    image aspect ratio.

    Parameters
    ----------
    imageSize : (int, int)
        The original image size
    targetQuadsSize : (int, int)
        The size of the quads the image will be split into
    margin : float
        The factory by which quads overlap
    maxReduction : float
        The produced size will never be less than the original size
        times maxReduction.

    Returns
    -------
    (width, height) : (int, int)
        The size the original image can be downscaled to
    """
    def roundDownIfCloseEnough(x, delta=.4):
        r = int(x)
        return r if x-r < delta else x

    w,h = imageSize
    tw,th = targetQuadsSize
    mw = int(margin*tw)
    mh = int(margin*th)
    sw,sh = tw-mw,th-mh

    rx = (w-mw)/sw
    ry = (h-mh)/sh
    r = min(rx, ry, 1/maxReduction)

    nw = w/r
    nh = h/r
    nw = mw + roundDownIfCloseEnough((nw-mw)/sw)*sw
    nh = mh + roundDownIfCloseEnough((nh-mh)/sh)*sh
    nw = max(int(nw), tw)
    nh = max(int(nh), th)

    return nw, nh


def projectOnDifferentScales(boxOrPosition, fromSize, toSize):
    """Project a box or a position from a 2D quad to another.

    The formula look like proj(x, a, b) = x/a*b

    Parameters
    ----------
    boxOrPosition : even numbered tuple or list
        For example, can be (x,y) or (x,y,w,h) or (x1,y1,x2,y2)
    fromSize : (int, int)
        The current bounds of boxOrPosition
    toSize : (int, int)
        The upscaled or downscaled bounds

    Returns
    -------
    [int] of size equal to that of boxOrPosition
        The scaled box or position
    """
    projected = list(boxOrPosition) # copy
    for i in range(0, len(projected), 2):
        projected[ i ] = int(projected[ i ]/fromSize[0]*toSize[0]) # scale on x axis
        projected[i+1] = int(projected[i+1]/fromSize[1]*toSize[1]) # scale on y axis
    return projected


def saveBoxesPicture(picture, boxes, name):
    """Devtool function, saves the given picture with rectangles to represent boxes"""
    import cv2
    # there is a color shift bug, I believe cv2 uses BGR instead of RGB
    image = np.asarray(picture)
    for x,y,w,h in boxes:
        cv2.rectangle(image, (x,y), (x+w,y+h), (252, 140, 255), thickness=8, lineType=cv2.LINE_AA)
    cv2.imwrite(name, image)