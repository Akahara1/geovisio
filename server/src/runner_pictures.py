import re
import datetime
from fs import open_fs
from PIL import Image, ExifTags
from flask import current_app
import psycopg
import traceback
import os
import io
import random
import gc
from psycopg.types.json import Jsonb
from collections import Counter
from src import pictures
from concurrent.futures import as_completed, ThreadPoolExecutor
from itertools import repeat

def run():
	"""Launch processing of pictures and sequences"""

	with open_fs(current_app.config['FS_URL']) as fs:
		with psycopg.connect(current_app.config['DB_URL']) as db:
			seqFolders = listSequencesToProcess(fs, db)

			if len(seqFolders) > 0:
				print(len(seqFolders), "sequences to process")

				for seqFolder in seqFolders:
					processSequence(fs, db, seqFolder)

				print("Done processing", len(seqFolders), "sequences")
			else:
				print("No sequence to process for now")


def cleanup():
	"""Removes database tables and deletes Geovisio derivated versions of pictures"""

	print("Cleaning up database...")
	with psycopg.connect(current_app.config['DB_URL']) as conn:
		sqlLines = open(os.path.join(os.path.dirname(__file__), './db_cleanup.sql'), "r").readlines()
		sqlLines = " ".join([
			re.sub("--.*", "", l.replace("\n", " "))
			for l in sqlLines
		])
		conn.execute(sqlLines)
		conn.commit()
		conn.close()

		print("Deleting derivated files...")
		with open_fs(current_app.config['FS_URL']) as fs:
			fs.removetree(current_app.config['PIC_DERIVATES_DIR'])

			print("Cleanup done")


def redoSequences(sequences):
	"""Reprocesses the files associated with the given sequences.

	This is a CLI entry point.

	Sequences entries are not modified in the database (except for there status),
	meaning uids are kept. The same is true for images, unless they are new or
	no longer exist. Sequences cannot be deleted this way.

	Because uids must remain the same, a simple cleanup()+process() cannot be used.

	*Beware*, this method does not update pictures metadata, meaning that if a
	sequence's picture files are modified, there must not be a new picture with
	the same name as an old one, otherwise the new one will have the same metadata,
	time stamp, position and heading in the database.

	Parameters
	----------
	sequences : [str]
		the sequences names to process again
	"""
	if len(sequences) == 0:
		print("No sequence given")
		return
	print("Re-processing", len(sequences), "sequences")
	with open_fs(current_app.config['FS_URL']) as fs:
		with psycopg.connect(current_app.config['DB_URL']) as db:
			for seq in sequences:
				if ('/' in seq) or (not fs.isdir("/" + seq)) or (seq.startswith("gvs_")):
					print("Cannot process non-sequence '%s'" % seq)
					continue
				storedSequence = db.execute("SELECT id FROM sequences where folder_path = %s", [ seq ]).fetchone()
				if storedSequence is None:
					print("Processing non stored sequence '%s'" % seq)
					processSequence(fs, db, seq)
				else:
					print("Reprocessing sequence '%s'" % seq)
					reprocessSequence(fs, db, seq, storedSequence[0])
			print("Done reprocessing %d sequences" % len(sequences))


def reprocessSequence(fs, db, sequenceFolder, sequenceId):
	"""Re-processes a sequence that already exists in the database.

	Derivate pictures are re-generated and the database is updated.

	IDs of existing picture entries are unmodified unless there images have been
	removed from the sequence, in which case `pictures` and `sequences_pictures`
	entries are removed.

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	pictureFilename : str
		Name of the picture file (relative to sequence folder)
	sequenceId : uuid
		The sequence's uuid, as stored in the database
	"""

	db.execute("UPDATE sequences SET status = 'preparing' WHERE id = %s", [ sequenceId ])
	db.commit()
	
	# collect all pictures to process for sequence

	newPictures = sorted([
		f for f in fs.listdir(sequenceFolder)
		if re.search(r'\.jpe?g$', f, re.IGNORECASE)
	])

	existingPictures = db.execute("""SELECT file_path
		FROM sequences_pictures INNER JOIN pictures ON (pictures.id = sequences_pictures.pic_id)
		WHERE seq_id = %s""", [ sequenceId ]).fetchall()

	allPictures = []
	allPictures.extend(newPictures)

	for (path,) in existingPictures:
		picName = path.split('/')[-1]
		if picName not in allPictures:
			allPictures.append(picName)
	
	# process pictures

	if current_app.config['BLUR_STRATEGY'] != "DISABLE":
		from src.blur.blur import blurPreinit
		blurPreinit(current_app.config)
	maxWorkers = getMaxWorkerThreads(current_app.config)
	print("Processing", len(newPictures), "pictures ( was", len(existingPictures), ") in sequence", sequenceFolder, "using", maxWorkers, "workers")

	remainingPicturesIds = []
	failCount = 0

	with ThreadPoolExecutor(max_workers=maxWorkers) as executor:
		picProcResults = executor.map(reprocessPicture, repeat(fs), repeat(db), repeat(sequenceFolder), allPictures, repeat(sequenceId), repeat(current_app.config))
		for result in picProcResults:
			if result['status'] == 'success':
				remainingPicturesIds.append(result['picId'])
			elif result['status'] == 'fail':
				failCount += 1

	# if all pictures failed, set the sequence status to 'broken' and stop
	if failCount > 0:
		print(failCount, "pictures failed in sequence", sequenceFolder)
	if len(remainingPicturesIds) == 0:
		print("Reprocess of sequence", sequenceFolder, "has failed (no valid picture remains)")
		db.execute("UPDATE sequences SET status = 'broken' WHERE id = %s", [ sequenceId ])
		db.commit()
		return
		
	# update ranks
	for i,pictureId in enumerate(remainingPicturesIds):
		db.execute("UPDATE sequences_pictures SET rank = %s WHERE pic_id = %s AND seq_id = %s", [ i+1, pictureId, sequenceId ])

	# update the sequence status
	db.execute("UPDATE sequences SET status = 'ready' WHERE id = %s", [ sequenceId ])
	db.commit()


def reprocessPicture(fs, db, sequenceFolder, pictureFilename, sequenceId, config):
	"""Reprocesses a picture.

	The picture file may or may not exist in the database and may or may not exist in the
	file system. It will be created/deleted/updated in the database and if it exists on disk
	its derivate files will be regenerated.

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	pictureFilename : str
		Name of the picture file (relative to sequence folder)
	sequenceId : uuid
		The sequence's uuid, as stored in the database
	config : dict
		Flask app.config (passed as param to allow using ThreadPoolExecutor)

	Returns
	-------
	{
		status: "fail" | "deleted" | "success",
		picId?: uuid
	}
	picIs is only specified if there is a picture entry in the database (it wasn't deleted/it was successfully created)
	"""
	picturePath = sequenceFolder + '/' + pictureFilename
	pictureExistsNow = fs.exists(picturePath)
	pictureAlreadyExists = False
	pictureId = None
	pictureObject = None
	randomPicRank = random.randint(1000, 10000000)

	existingEntry = db.execute('SELECT id FROM pictures WHERE file_path = %s', [ picturePath ]).fetchall()
	if len(existingEntry) != 0:
		pictureId = existingEntry[0][0]
		pictureAlreadyExists = True

	if pictureExistsNow:
		pictureObject = Image.open(fs.openbin(picturePath))

	if pictureExistsNow and pictureAlreadyExists:
		# only reprocess files
		db.execute("UPDATE pictures SET status = 'preparing' WHERE id = %s", [ pictureId ])
		db.execute("UPDATE sequences_pictures SET rank = %s WHERE pic_id = %s", [ randomPicRank, pictureId ])
		print("Updated picture", pictureFilename, "in sequence", sequenceFolder)
	elif pictureExistsNow:
		# insert in database
		try:
			pictureId = insertNewPictureInDatabase(db, sequenceFolder, pictureFilename, pictureObject)
			db.execute("INSERT INTO sequences_pictures (seq_id, rank, pic_id) VALUES (%s, %s, %s)", [ sequenceId, randomPicRank, pictureId ])
			print("New picture", pictureFilename, "in sequence", sequenceFolder)
		except Exception as e:
			printPictureProcessingError(e, sequenceFolder, pictureFilename)
			return { 'status': 'fail' }
	else:
		# remove from database and remove generate files
		db.execute("DELETE FROM sequences_pictures WHERE seq_id = %s AND pic_id = %s", [ sequenceId, pictureId ])
		db.execute("DELETE FROM pictures WHERE file_path = %s", [ picturePath ])
		picDerivatesParent = f"{config['PIC_DERIVATES_DIR']}/{str(pictureId)[0:2]}"
		picDerivatesFolder = f"{picDerivatesParent}/{pictureId}"
		if fs.exists(picDerivatesFolder):
			fs.removetree(picDerivatesFolder)
			if fs.isempty(picDerivatesParent):
				fs.removedir(picDerivatesParent)
		print("Removed picture", pictureFilename, "in sequence", sequenceFolder)
		return { 'status': 'deleted' }
	
	try:
		processPictureFiles(fs, pictureId, pictureObject, config)
		db.execute("UPDATE pictures SET status = 'ready' WHERE id = %s", [ pictureId ])

		return { 'status': 'success', 'picId': pictureId }
	except Exception as e:
		printPictureProcessingError(e, sequenceFolder, pictureFilename)
		db.execute("UPDATE pictures SET status = 'broken' WHERE id = %s", [ pictureId ])
		return { 'status': 'fail', 'picId': pictureId }


def listSequencesToProcess(fs, db):
	"""Find new sequence folders to process

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection

	Returns
	-------
	list
		List of sequence folders to import
	"""

	sequencesFolders = [ x for x in fs.listdir("/") if fs.isdir("/" + x) and not x.startswith("gvs_") ]
	sequencesInDb = db.execute("SELECT array_agg(folder_path) FROM sequences").fetchone()[0]
	diff = Counter(sequencesFolders) - Counter(sequencesInDb)
	return list(diff.elements())


def processSequence(fs, db, sequenceFolder):
	"""Processes a single sequence (list pictures, read metadata, populate database)

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	"""

	# List available pictures in sequence folder
	picturesFilenames = sorted([
		f for f in fs.listdir(sequenceFolder)
		if re.search(r'\.jpe?g$', f, re.IGNORECASE)
	])

	if len(picturesFilenames) > 0:
		# Create sequence in database
		seqId = db.execute("INSERT INTO sequences(folder_path) VALUES(%s) RETURNING id", [sequenceFolder]).fetchone()[0]

		# Process pictures
		maxWorkers = getMaxWorkerThreads(current_app.config)
		print("Processing", len(picturesFilenames), "pictures in sequence", sequenceFolder, "using", maxWorkers, "workers")
		picProcessFailures = 0
		picIds = []

		if current_app.config['BLUR_STRATEGY'] != "DISABLE":
			from src.blur.blur import blurPreinit
			blurPreinit(current_app.config)

		with ThreadPoolExecutor(max_workers=maxWorkers) as executor:
			picProcResults = executor.map(processNewPicture, repeat(fs), repeat(db), repeat(sequenceFolder), picturesFilenames, repeat(current_app.config), repeat(True))

			for picProcRes in picProcResults:
				if picProcRes['status'] == "broken":
					picProcessFailures += 1
				else:
					picIds.append(picProcRes['id'])

			print("") # For line return after pictures dots display

		if picProcessFailures > 0:
			print(picProcessFailures, "pictures failed in sequence", sequenceFolder)

		if picProcessFailures == len(picturesFilenames):
			# Remove sequence if all its pictures failed
			db.execute("DELETE FROM sequences WHERE id = %s", [seqId])
			db.commit()
			print("Import of sequence", sequenceFolder,"is cancelled (all pictures failed)")
		else:
			# Update sequence metadata and status
			# Create link between pictures and sequence
			with db.cursor() as cursor:
				with cursor.copy("COPY sequences_pictures(seq_id, rank, pic_id) FROM STDIN") as copy:
					for i, p in enumerate(picIds):
						copy.write_row((seqId, i+1, p))

			# Process field of view for each pictures
			picMake, picModel, picType = db.execute("""
				SELECT metadata->>'make' AS make, metadata->>'model' AS model, metadata->>'type' AS type
				FROM pictures p
				JOIN sequences_pictures sp ON sp.seq_id = %s AND p.id = sp.pic_id AND rank = 1
			""", [seqId]).fetchone()

			# Flat pictures = variable fov
			if picType == 'flat':
				if picMake is not None and picModel is not None:
					db.execute("SET pg_trgm.similarity_threshold = 0.9");
					db.execute("""
						UPDATE pictures
						SET metadata = jsonb_set(metadata, '{field_of_view}'::text[], COALESCE(
							(
								SELECT ROUND(DEGREES(2 * ATAN(sensor_width / (2 * (metadata->>'focal_length')::float))))::varchar
								FROM cameras
								WHERE model %% CONCAT(%(make)s::text, ' ', %(model)s::text)
								ORDER BY model <-> CONCAT(%(make)s::text, ' ', %(model)s::text)
								LIMIT 1
							),
							'null'
						)::jsonb)
						WHERE id = ANY (SELECT pic_id FROM sequences_pictures WHERE seq_id = %(seq)s)
					""", { "seq": seqId, "make": picMake, "model": picModel });

			# 360 pictures = 360° fov
			else:
				db.execute("""
					UPDATE pictures
					SET metadata = jsonb_set(metadata, '{field_of_view}'::text[], '360'::jsonb)
					WHERE id = ANY (SELECT pic_id FROM sequences_pictures WHERE seq_id = %s)
				""", [seqId])

			# Change sequence database status in DB
			db.execute("""
				UPDATE sequences
				SET status = 'ready', geom = ST_MakeLine(ARRAY(
					SELECT p.geom
					FROM sequences_pictures sp
					JOIN pictures p ON sp.pic_id = p.id
					WHERE sp.seq_id = %(seq)s
					ORDER BY sp.rank
				))
				WHERE id = %(seq)s
			""", { "seq": seqId })
			db.commit()
			print("Sequence", sequenceFolder, "is ready")
	else:
		print("Skipped empty sequence", sequenceFolder)
		

def processNewPicture(fs, db, sequenceFolder, pictureFilename, config, dots = False):
	"""Processes a single picture (read file, extract metadata, generate derivate files, insert in database)

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	pictureFilename : str
		Name of the picture file (relative to sequence folder)
	config : dict
		Flask app.config (passed as param to allow using ThreadPoolExecutor)

	Returns
	-------
	{ id : uuid, status : "ready"|"broken" }
		if a new picture entry was successfully added in the database
	False
		if the picture could not be added to the database (because it has
		no exif info for example)
	"""

	picId = None
	picProcessStatus = None

	try:
		picture = Image.open(fs.openbin(sequenceFolder + "/" + pictureFilename))

		# create the picture entry in the database
		picId = insertNewPictureInDatabase(db, sequenceFolder, pictureFilename, picture)
		picProcessStatus = "preparing"

		# Generate derivated picture versions
		processPictureFiles(fs, picId, picture, config)
		picProcessStatus = "ready"

		if dots:
			print(".", end='', flush=True)

	except Exception as e:
		picProcessStatus = "broken"
		printPictureProcessingError(e, sequenceFolder, pictureFilename)

	# Mark picture as ready in database
	finally:
		if picId is not None:
			db.execute("UPDATE pictures SET status = %s WHERE id = %s", (picProcessStatus, picId))
		return { "id": picId, "status": picProcessStatus }


def insertNewPictureInDatabase(db, sequenceFolder, pictureFilename, picture):
	"""Inserts a new 'pictures' entry in the database, from a picture file

	Parameters
	----------
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	pictureFilename : str
		Name of the picture file (relative to sequence folder)

	Returns
	-------
	uuid : The uuid of the new picture entry in the database
	"""

	# Create a fully-featured metadata object
	metadata = readPictureMetadata(picture) | pictures.getPictureSizing(picture)

	# Remove cols/rows information for flat pictures
	if metadata["type"] == "flat":
		metadata.pop("cols")
		metadata.pop("rows")

	# Add picture metadata to database (with "preparing" status)
	picId = db.execute("""
		INSERT INTO pictures (file_path, ts, heading, metadata, geom)
		VALUES (%s, to_timestamp(%s), %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326))
		RETURNING id
	""", (
		sequenceFolder + "/" + pictureFilename,
		metadata['ts'],
		metadata['heading'],
		Jsonb(metadata),
		metadata['lon'],
		metadata['lat']
	)).fetchone()[0]

	return picId


def processPictureFiles(fs, picId, picture, config):
	"""Generates the files associated with a sequence picture.
	
	If needed the image is blurred before the tiles and thumbnail are generated.

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picId : uuid
		The picture's uuid, as stored in the database
	picture : PIL.Image
		The picture to process
	config : dict
		Flask app.config (passed as param to allow using ThreadPoolExecutor)
	"""
	metadata = readPictureMetadata(picture) | pictures.getPictureSizing(picture)

	picDerivatesFolder = f"{config['PIC_DERIVATES_DIR']}/{str(picId)[0:2]}/{picId}"
	fs.makedirs(picDerivatesFolder, recreate=True)

	# Create blurred version if required
	if config['BLUR_STRATEGY'] != "DISABLE":
		from src.blur.blur import blurPicture
		picture = blurPicture(picture) # Swap original picture and blurred version
		blurredBytes = io.BytesIO()
		picture.save(blurredBytes, format="jpeg", quality=90, subsampling=0)
		fs.writebytes(picDerivatesFolder + "/blurred.jpg", blurredBytes.getvalue())
		# For some reason garbage collection does not run automatically after
		# a call to an AI model, so it must be done explicitely
		gc.collect()

	# Create SD, thumbnail and tiles
	generatePictureDerivates(fs, picture, metadata, picDerivatesFolder, metadata["type"])


def readPictureMetadata(picture):
	"""Extracts metadata from picture file

	Parameters
	----------
	picture : PIL.Image
		Picture file

	Returns
	-------
	dict
		Various metadata fields : lat, lon, ts, heading, type, make, model, focal_length
	"""

	data = {}

	info = picture._getexif()
	if info:
		for tag, value in info.items():
			decoded = ExifTags.TAGS.get(tag, tag)
			if decoded == "GPSInfo":
				for t in value:
					sub_decoded = ExifTags.GPSTAGS.get(t, t)
					data[sub_decoded] = value[t]
			else:
				data[decoded] = value

	# Parse latitude/longitude
	if 'GPSLatitude' in data:
		latRaw = data['GPSLatitude']
		lat = (-1 if data['GPSLatitudeRef'] == 'S' else 1) * (float(latRaw[0]) + float(latRaw[1]) / 60 + float(latRaw[2]) / 3600)

		lonRaw = data['GPSLongitude']
		lon = (-1 if data['GPSLongitudeRef'] == 'W' else 1) * (float(lonRaw[0]) + float(lonRaw[1]) / 60 + float(lonRaw[2]) / 3600)
	else:
		raise Exception("No GPS coordinates in picture EXIF tags")

	# Parse date/time
	if 'GPSTimeStamp' in data:
		timeRaw = data['GPSTimeStamp']
		dateRaw = data['GPSDateStamp'].replace(":", "-")
		msRaw = data['SubSecTimeOriginal'] if 'SubSecTimeOriginal' in data else "0"
		d = datetime.datetime.combine(
			datetime.date.fromisoformat(dateRaw),
			datetime.time(
				int(timeRaw[0]),
				int(timeRaw[1]),
				int(timeRaw[2]),
				int(msRaw[:6].ljust(6, "0")),
				tzinfo=datetime.timezone.utc
			)
		)
	elif 'DateTimeOriginal' in data:
		dateRaw = data['DateTimeOriginal'].split(" ")[0].replace(":", "-")
		timeRaw = data['DateTimeOriginal'].split(" ")[1].split(":")
		msRaw = data['SubSecTimeOriginal'] if 'SubSecTimeOriginal' in data else "0"
		d = datetime.datetime.combine(
			datetime.date.fromisoformat(dateRaw),
			datetime.time(
				int(timeRaw[0]),
				int(timeRaw[1]),
				int(timeRaw[2]),
				int(msRaw[:6].ljust(6, "0")),
				tzinfo=datetime.timezone.utc
			)
		)
	else:
		raise Exception("No date in picture EXIF tags")

	# Projection type (equirectangular)
	for segment, content in picture.applist:
		marker, body = content.split(b'\x00', 1)
		if segment == 'APP1' and marker == b'http://ns.adobe.com/xap/1.0/':
			rgxm = re.search("<GPano:ProjectionType>.*</GPano:ProjectionType>", str(body))
			if rgxm:
				data["ProjectionType"] = rgxm.group()[22:-23]

	if 'GPSImgDirection' not in data:
		raise Exception("No image direction in picture EXIF tags")

	# Make and model
	make = decode_make_model(data['Make']).strip() if 'Make' in data else None
	model = decode_make_model(data['Model']).strip() if 'Model' in data else None
	if model is not None and model is not None:
		model = model.replace(make, "").strip()

	return {
		"lat": lat,
		"lon": lon,
		"ts": d.timestamp(),
		"heading": int(round(data['GPSImgDirection'])),
		"type": data['ProjectionType'] if "ProjectionType" in data else "flat",
		"make": make,
		"model": model,
		"focal_length": float(data['FocalLength']) if 'FocalLength' in data else None
	}


def generatePictureDerivates(fs, picture, sizing, outputFolder, type = "equirectangular"):
	"""Creates all derivated version of a picture (thumbnail, small, tiled)

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Picture file
	sizing : dict
		Picture dimensions (width, height, cols, rows)
	outputFolder : str
		Path to output folder (relative to instance root)
	type : str (optional)
		Type of picture (flat, equirectangular (default))

	Returns
	-------
	bool
		True if worked
	"""

	# Thumbnail + fixed-with versions
	pictures.createThumbPicture(fs, picture, outputFolder + "/thumb.jpg", type)
	pictures.createSDPicture(fs, picture, outputFolder + "/sd.jpg")

	# Progressive HD picture
	if type == "flat":
		pictures.createProgressiveHDPicture(fs, picture, outputFolder + "/progressive.jpg")
	# Tiles
	else:
		tileFolder = outputFolder + "/tiles"
		fs.makedir(tileFolder, recreate=True)
		pictures.createTiledPicture(fs, picture, tileFolder, sizing["cols"], sizing["rows"])

	return True


def decode_make_model(value):
	"""Python 2/3 compatible decoding of make/model field."""
	if hasattr(value, "decode"):
		try:
			return value.decode("utf-8").replace('\x00','')
		except UnicodeDecodeError:
			return None
	else:
		return value.replace('\x00','')


def getMaxWorkerThreads(config):
	"""Dictates how many worker threads can be allocated to the processing of a sequence.

	This number may vary depending on the blur configuration and the BLUR_THREADS_LIMIT environment variable.

	Returns
	-------
	int
		The number of threads to use to process a sequence, always less than the number of available CPUs
	"""
	if config['BLUR_STRATEGY'] == 'DISABLE' or config['BLUR_THREADS_LIMIT'] == 0:
		return os.cpu_count()
	return min(config['BLUR_THREADS_LIMIT'], os.cpu_count())


def printPictureProcessingError(e, sequenceFolder, pictureFilename):
	"""Prints a decorated error on stdout.

	Parameters
	----------
	e : Error
		The error that caused the processing to stop
	sequenceFolder : str
		The sequence folder containing the picture that broke
	pictureFilename : str
		The picture's file name that couldn't be processed
	"""
	print("")
	print("---------------------------------------------------------")
	print("WARNING : an error occured while processing picture '%s/%s'" % (sequenceFolder, pictureFilename))
	print(''.join(traceback.format_exception(type(e), e, e.__traceback__)))
	print("---------------------------------------------------------")
	print("")